package ru.t1.artamonov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final TaskDTO task = taskEndpointClient.getTaskById(request).getTask();
        showTask(task);
    }

}
