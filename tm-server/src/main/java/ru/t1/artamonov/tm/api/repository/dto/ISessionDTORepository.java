package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.dto.model.SessionDTO;


@Repository
public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

    @Transactional
    void deleteByUserId(String userId);

    @Nullable
    SessionDTO findByUserIdAndId(String userId, String id);

}
