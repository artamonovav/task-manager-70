package ru.t1.artamonov.tm.api.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

}
